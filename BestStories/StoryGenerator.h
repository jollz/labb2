//
//  StoryGenerator.h
//  BestStories
//
//  Created by dronnefjord on 2015-01-29.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoryGenerator : NSObject

- (NSString*)generateStoryWithGenre:(NSString*)genre protagonistsGender:(NSString*)gender andOutcome:(NSString*)outcome;

@end
