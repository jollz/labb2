//
//  ViewController.m
//  BestStories
//
//  Created by dronnefjord on 2015-01-27.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import "ViewController.h"
#import "StoryViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSwitch;
@property (weak, nonatomic) IBOutlet UISegmentedControl *endingSwitch;

@end

@implementation ViewController

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"toStory"]){
        StoryViewController *controller = (StoryViewController *)segue.destinationViewController;
        controller.genre = [self pickerView:self.pickerView titleForRow:[self.pickerView selectedRowInComponent:0] forComponent:0];
        controller.gender = [self.genderSwitch titleForSegmentAtIndex:self.genderSwitch.selectedSegmentIndex];
        controller.outcome = [self.endingSwitch titleForSegmentAtIndex:self.endingSwitch.selectedSegmentIndex];
    }
}

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 2;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    switch (row){
        case 0:
            return @"Fantasy";
            break;
        case 1:
            return @"Sci-fi";
            break;
        default:
            return @"";
            break;		
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
