//
//  ViewController.h
//  BestStories
//
//  Created by dronnefjord on 2015-01-27.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>


@end

