//
//  StoryViewController.h
//  BestStories
//
//  Created by dronnefjord on 2015-01-29.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoryViewController : UIViewController

@property (nonatomic) NSString* genre;
@property (nonatomic) NSString* gender;
@property (nonatomic) NSString* outcome;

@end
