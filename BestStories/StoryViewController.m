//
//  StoryViewController.m
//  BestStories
//
//  Created by dronnefjord on 2015-01-29.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import "StoryViewController.h"
#import "StoryGenerator.h"

@interface StoryViewController ()

@property (weak, nonatomic) IBOutlet UITextView *storyText;

@end

@implementation StoryViewController {
    StoryGenerator *generator;
}

- (IBAction)newStory:(id)sender {
    self.storyText.text = [generator generateStoryWithGenre:self.genre protagonistsGender:self.gender andOutcome:self.outcome];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    generator = [[StoryGenerator alloc] init];
    
    self.storyText.text = [generator generateStoryWithGenre:self.genre protagonistsGender:self.gender andOutcome:self.outcome];
}

@end
