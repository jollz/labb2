//
//  StoryGenerator.m
//  BestStories
//
//  Created by dronnefjord on 2015-01-29.
//  Copyright (c) 2015 dronnefjord. All rights reserved.
//

#import "StoryGenerator.h"

@interface StoryGenerator ()

@property (nonatomic) NSString* selectedRace;
@property (nonatomic) NSString* selectedName;
@property (nonatomic) NSString* selectedOrigin;
@property (nonatomic) NSString* gender;

@end

@implementation StoryGenerator
- (NSString*)generateStoryWithGenre:(NSString*)genre protagonistsGender:(NSString*)gender andOutcome:(NSString*)outcome{
    
    NSArray* fantasyRaces = @[@"Elf", @"Dwarf", @"Human"];
    NSArray* maleDwarfNames = @[@"Yudmuil", @"Korgoir", @"Reirbaic", @"Dumnulim", @"Londerlug"];
    NSArray* femaleDwarfNames = @[@"Thrataebera", @"Vafriren", @"Dummoula", @"Netdrilin", @"Nebegaer"];
    NSArray* maleElfNames = @[@"Ehrendil", @"Halamar", @"Raeranthur", @"Quynn", @"Erendriel"];
    NSArray* femaleElfNames = @[@"Laerdya", @"Hamalitia", @"Keishara", @"Naesala", @"Ildilyntra"];
    NSArray* maleHumanNames = @[@"Nathaniel", @"Arley", @"Malcolm"];
    NSArray* femaleHumanNames = @[@"Maegan", @"Miah", @"Karlie"];
    
    NSArray* dwarfLocationOfOrigin = @[@"Daggerspine Mountains", @"Bouldercrag Valley"];
    NSArray* elfLocationOfOrigin = @[@"Gloomy Wilds", @"Everbloom Forest", @"Calm Groove"];
    NSArray* humanLocationOfOrigin = @[@"Windy Plains",@"Shimmering Meadow",@"Whispering Savanna"];
    NSArray* scifiLocationOfOrigin = @[@"Planet Z13", @"Planet U5X"];
    
    if([genre isEqualToString:@"Fantasy"]){
        self.selectedRace = fantasyRaces[arc4random() % fantasyRaces.count];
        if([gender isEqualToString:@"Male"]){
            if([self.selectedRace isEqualToString:@"Dwarf"]){
                self.selectedName = maleDwarfNames[arc4random() % maleDwarfNames.count];
                self.selectedOrigin = dwarfLocationOfOrigin[arc4random() % dwarfLocationOfOrigin.count];
            } else if([self.selectedRace isEqualToString:@"Elf"]){
                self.selectedName = maleElfNames[arc4random() % maleElfNames.count];
                self.selectedOrigin = elfLocationOfOrigin[arc4random() % elfLocationOfOrigin.count];
            } else {
                self.selectedName = maleHumanNames[arc4random() % maleHumanNames.count];
                self.selectedOrigin = humanLocationOfOrigin[arc4random() % humanLocationOfOrigin.count];
            }
        } else {
            if([self.selectedRace isEqualToString:@"Dwarf"]){
                self.selectedName = femaleDwarfNames[arc4random() % femaleDwarfNames.count];
                self.selectedOrigin = dwarfLocationOfOrigin[arc4random() % dwarfLocationOfOrigin.count];
            } else if([self.selectedRace isEqualToString:@"Elf"]){
                self.selectedName = femaleElfNames[arc4random() % femaleElfNames.count];
                self.selectedOrigin = elfLocationOfOrigin[arc4random() % elfLocationOfOrigin.count];
            } else {
                self.selectedName = femaleHumanNames[arc4random() % femaleHumanNames.count];
                self.selectedOrigin = humanLocationOfOrigin[arc4random() % humanLocationOfOrigin.count];
            }
        }
    } else if([genre isEqualToString:@"Sci-fi"]){
        self.selectedRace = @"Human";
        if([gender isEqualToString:@"Male"]){
            self.selectedName = maleHumanNames[arc4random() % maleHumanNames.count];
            self.selectedOrigin = scifiLocationOfOrigin[arc4random() % scifiLocationOfOrigin.count];
        } else {
            self.selectedName = femaleHumanNames[arc4random() % femaleHumanNames.count];
            self.selectedOrigin = scifiLocationOfOrigin[arc4random() % scifiLocationOfOrigin.count];
        }
    }
    
    NSString* story = [NSString stringWithFormat:@"%@ - %@ - %@",
                       [self generateBeginningForGenre:genre],
                       [self generateStoryBlockForGenre:genre],
                       [self generateEndingForGenre:genre withOutcome:outcome]];
    return story;
}

-(NSString*)generateBeginningForGenre:(NSString*)genre{
    if([genre isEqualToString:@"Fantasy"]){
        NSArray* beginnings = @[[NSString stringWithFormat:@"In a different world. A world filled with magic and wonder, lived a %@ called %@. %@'s people lived in the %@. %@ was curious about the surrounding world and set out on a journey to learn the way of the world.",
                                 self.selectedRace,
                                 self.selectedName,
                                 self.selectedName,
                                 self.selectedOrigin,
                                 self.selectedName],
                                [NSString stringWithFormat:@"This story follows a young %@ by the name of %@. %@ had set out on a journey to explore the world around the %@ - the home of %@'s people.",
                                self.selectedRace,
                                self.selectedName,
                                self.selectedName,
                                self.selectedOrigin,
                                self.selectedName]];
        return beginnings[arc4random() % beginnings.count];
    } else {
        NSArray* enemyFactions = @[@"Sheuurian Empire", @"Ienochion Alliance", @"Acswiri Protectorate",];
        NSArray* beginnings = @[[NSString stringWithFormat:@"Battered after last night's battle with the %@, %@ had a hard time getting out of bed. %@ was one of the few remaining %@s in the galaxy. Sitting at the helm of the small spacecraft, %@ was wondering where to go.",
                                 enemyFactions[arc4random() % enemyFactions.count],
                                 self.selectedName,
                                 self.selectedName,
                                 self.selectedRace,
                                 self.selectedName]];
        return beginnings[arc4random() % beginnings.count];
   }
}

-(NSString*)generateStoryBlockForGenre:(NSString*)genre{
    if([genre isEqualToString:@"Fantasy"]){
        NSArray* targetLocations = @[@"Duncaster",@"Calcherth",@"Troutbeck",@"Azmar"];
        NSArray* firstDestination = targetLocations[arc4random() % targetLocations.count];
        NSArray* blocks = @[[NSString stringWithFormat:@"The first destination of %@'s journey was %@, a small town not far from the %@. When %@ reaches %@, %@ has planned to gather information about the next destination, %@.",
                             self.selectedName,
                             firstDestination,
                             self.selectedOrigin,
                             self.selectedName,
                             firstDestination,
                             self.selectedName,
                             targetLocations[arc4random() % targetLocations.count]]];
        
        return blocks[arc4random() % blocks.count];
    } else {
        NSArray* targetPlanets = @[@"Etrides", @"Askorix", @"Eyfreon"];
        NSArray* targetCities = @[@"Echo Bay", @"North Fork Spaceharbor"];
        NSArray* blocks = @[[NSString stringWithFormat:@"%@ has a certain destination in mind, but in order to get there %@ has to restock on supplies. A short pit stop will be made at %@ on planet %@",
                             self.selectedName,
                             self.selectedName,
                             targetCities[arc4random() % targetCities.count],
                             targetPlanets[arc4random() % targetPlanets.count]]];
        
        return blocks[arc4random() % blocks.count];
    }
}

-(NSString*)generateEndingForGenre:(NSString*)genre withOutcome:(NSString*)outcome{
    if([genre isEqualToString:@"Fantasy"]){
        if([outcome isEqualToString:@"Bad"]){
            NSArray* enemy = @[@"Savage Wolf", @"Giant Bear", @"Bandit"];
            NSArray* endings = @[[NSString stringWithFormat:@"%@ was following the path to the next destination of %@'s journey. Little did %@ know what was lurking in the bushes. Without warning a %@ leaped out of the bushes and with a quick slash towards %@'s throat severed %@'s head from the body ending %@'s journey shortly after it began.",
                                  self.selectedName,
                                  self.selectedName,
                                  self.selectedName,
                                  enemy[arc4random() % enemy.count],
                                  self.selectedName,
                                  self.selectedName,
                                  self.selectedName]];
            
            return endings[arc4random() % endings.count];
        } else {
            NSArray* wizardNames = @[@"Uqor",@"Ererius",@"Ouodel",@"Kejahr"];
            NSArray* endings = @[[NSString stringWithFormat:@"%@ was on the way to the next destination of %@'s journey. Everything was going according to plan. At a crossroads just ahead a strange figure reveals itself. It's a man dressed in a weird outfit. %@ introduces himself and the strange man replies:'I am %@, the wizard'. And the two become friends and ventures together for the remainder of the journey.",
                                  self.selectedName,
                                  self.selectedName,
                                  self.selectedName,
                                  wizardNames[arc4random() % wizardNames.count]]];
            
            return endings[arc4random() % endings.count];
        }
    } else {
        if([outcome isEqualToString:@"Bad"]){
            NSArray* endings = @[[NSString stringWithFormat:@"After the pit stop %@ changed course towards the next destination, %@. %@ is the last remaining home of the %@'s and the home of %@. Suddenly, the radar beeped and %@ froze. Out of nowhere two torpedos appeared and %@ had no chance to avoid them. The torpedos hit, and the small spacecraft's shields couldn't withstand the force of the explosions. Nothing was left but floating debris.",
                                  self.selectedName,
                                  self.selectedOrigin,
                                  self.selectedOrigin,
                                  self.selectedRace,
                                  self.selectedName,
                                  self.selectedName,
                                  self.selectedName]];
            return endings[arc4random() % endings.count];
        } else {
            NSArray* friendsName = @[@"Melton", @"Smokey", @"Daisey", @"Fruitcake"];
            NSString* selectedFriend = friendsName[arc4random() % friendsName.count];
            NSArray* endings = @[[NSString stringWithFormat:@"After the pit stop %@ changed course towards the next destination, %@. %@ is the last remaining home of the %@'s and the home of %@. Suddenly, the radar beeped and %@ started smiling. A familiar vessel appeared. It was %@'s good friend %@. %@ was also headed towards %@ and they got into formation and flew there together.",
                                  self.selectedName,
                                  self.selectedOrigin,
                                  self.selectedOrigin,
                                  self.selectedRace,
                                  self.selectedName,
                                  self.selectedName,
                                  self.selectedName,
                                  selectedFriend,
                                  selectedFriend,
                                  self.selectedOrigin]];
            return endings[arc4random() % endings.count];
        }
    }
}

@end
